import imaplib
import yaml
import email
import pdfkit
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


def remove_spaces(s):
    return "".join(s.split())


def make_pdf_attachment(file_path):
    part = MIMEBase('application', 'octet-stream')
    with open(file_path, "rb") as attachment:
        part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header(
        'Content-Disposition',
        "attachment; filename= %s" % file_path)
    return part

with open("login.yaml", 'r') as stream:
    creds = yaml.load(stream)

with open(".last_mail.yaml", 'r') as stream:
    last_mail = yaml.load(stream)

with open(".sent.yaml", 'r') as stream:
    sent = yaml.load(stream) or {}

print("Last mail checked: ", last_mail)

password = creds["password"]
email_address = creds["email"]
handler_email = creds["handler_email"]
imapAddress = creds["imapAddress"]
smtpAddress = creds["smtpAddress"]
label = creds["label"]

imap_server = imaplib.IMAP4_SSL(imapAddress)
smtp_server = smtplib.SMTP(smtpAddress, 587)
smtp_server.starttls()

smtp_server.login(email_address, password)
imap_server.login(email_address, password)
imap_server.select(label)

if last_mail is not None:
    last_mail_date = "-".join(last_mail.split()[1:4])
    query = '(SINCE "' + str(last_mail_date) + '")'
    result, data = imap_server.search(None, query)
else:
    result, data = imap_server.search(None, "ALL")

ids = data[0]
id_list = ids.split()

for mail_id in id_list:
    result, data = imap_server.fetch(mail_id, "(RFC822)")
    raw_email = data[0][1]
    msg = email.message_from_bytes(raw_email)

    mail_message_id = msg.get("Message-ID")
    mail_subject = msg.get("Subject")
    mail_type = msg.get_default_type()
    mail_content_type = msg.get_content_type()
    mail_date = msg.get("Date")
    mail_from = msg.get('From') \
        .split(" ")[-1] \
        .replace("<", "") \
        .replace(">", "")
    mail_date_time = email.utils.parsedate_to_datetime(mail_date)
    mail_timestamp = str(mail_date_time.timestamp()).replace(".0", "")
    mail_subject_nospace = remove_spaces(mail_subject)
    mail_name = "%s-%s-%s" % (mail_from, mail_timestamp, mail_subject_nospace)
    mail_path = "./mails/" + mail_name

    with open(".last_mail.yaml", "w") as f:
        f.write(mail_date)

    if mail_message_id in sent:
        continue

    print(mail_date, mail_content_type, mail_from, mail_subject)

    if not msg.is_multipart():
        continue

    plain_body = msg.get_payload(0)

    try:
        html_body = msg.get_payload(1)
    except Exception as e:
        print("Skipping", mail_subject, e)
        continue

    body = str(html_body.as_string()) \
        .replace("=\r\n", "") \
        .replace("=\n", "") \
        .replace('=3D"', '="') \
        .replace("=20", "") \
        .replace("=09", "") \
        .replace("=C2=A0", " ") \
        .replace("=E2=80=94", "—") \
        .replace("=C3=B8", "ø")

    style = "background: white;" +\
        "position: relative;" +\
        ""

    body_white = "<div style='%s'>%s</div>" % (style, body)

    pdf_file_path = mail_path + ".pdf"
    try:
        pdfkit.from_string(body_white, pdf_file_path, options={
            'quiet': None,
            # 'no-background': None,
            'no-outline': None,
            'encoding': "UTF-8",
            'user-style-sheet': 'styles.css',
            'zoom': 0.99
        })
    except Exception as e:
        print(e)

    forwarded_mail = MIMEMultipart()
    forwarded_mail['From'] = email_address
    forwarded_mail['To'] = handler_email
    forwarded_mail['Subject'] = mail_name
    forwarded_mail.attach(make_pdf_attachment(pdf_file_path))

    should_send = input("Send this? Y/n").lower()

    if should_send == "y" or should_send == "":
        print("Sending", mail_name, "to", handler_email)
        smtp_server.sendmail(
            email_address,
            handler_email,
            forwarded_mail.as_string())

    sent[mail_message_id] = 1

with open(".sent.yaml", "w") as f:
    f.write(yaml.dump(sent))

smtp_server.quit()
