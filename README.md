Invoice mailer
=================

Helps you convert plain emails to PDFs!

Sends plain emails from one inbox to a chosen target.



### Install

1. `$ sudo apt-get install wkhtmltopdf`
2. `pip install -r requirements.txt`

### Setup

Fill in login.yaml (copy login.example.yaml):

**email** the mail address you store plain emails  
**password** password to email  
**label** label to search for  
**handler_email** target mail address where you want pdfs to go  
**imapAddress** imap server of your email  
**smtpAddress** smtp server of your email  

### Usage

`python main.py`

